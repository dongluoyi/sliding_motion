import 'package:flutter_test/flutter_test.dart';
import 'package:sliding_motion/sliding_motion.dart';
import 'package:sliding_motion/sliding_motion_platform_interface.dart';
import 'package:sliding_motion/sliding_motion_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockSlidingMotionPlatform
    with MockPlatformInterfaceMixin
    implements SlidingMotionPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final SlidingMotionPlatform initialPlatform = SlidingMotionPlatform.instance;

  test('$MethodChannelSlidingMotion is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelSlidingMotion>());
  });

  test('getPlatformVersion', () async {
    SlidingMotion slidingMotionPlugin = SlidingMotion();
    MockSlidingMotionPlatform fakePlatform = MockSlidingMotionPlatform();
    SlidingMotionPlatform.instance = fakePlatform;

    expect(await slidingMotionPlugin.getPlatformVersion(), '42');
  });
}
