import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'sliding_motion_method_channel.dart';

abstract class SlidingMotionPlatform extends PlatformInterface {
  /// Constructs a SlidingMotionPlatform.
  SlidingMotionPlatform() : super(token: _token);

  static final Object _token = Object();

  static SlidingMotionPlatform _instance = MethodChannelSlidingMotion();

  /// The default instance of [SlidingMotionPlatform] to use.
  ///
  /// Defaults to [MethodChannelSlidingMotion].
  static SlidingMotionPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [SlidingMotionPlatform] when
  /// they register themselves.
  static set instance(SlidingMotionPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
