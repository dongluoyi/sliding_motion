import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'sliding_motion_platform_interface.dart';

/// An implementation of [SlidingMotionPlatform] that uses method channels.
class MethodChannelSlidingMotion extends SlidingMotionPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('sliding_motion');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
