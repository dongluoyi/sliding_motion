import 'package:flutter/material.dart';
import 'dart:math';

class AppColor {
  AppColor._();

  static const whiteColor = Color(0xFFFFFFFF);
  static const blackColor = Color(0xFF000000);
  static const clearColor = Color(0x00000000);

  static const Color color333333 = Color(0xff333333);
  static const Color color666666 = Color(0xff666666);
  static const Color color999999 = Color(0xff999999);

  static const Color dialogCancelTextColor = Color(0xFFBDBDBD);
  static const Color halfClearColorBlack = Color.fromARGB(77, 0, 0, 0);
  static const Color halfClearColorWhite = Color(0xccffffff);
  static const Color disableCoverColor = Color(0x11ffffff);

  static const List<Color> gradientColor1 = [
    Color(0x99283F63),
    Color(0x995180C9)
  ];

  static Color randomColor = Color.fromARGB(
      255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255));
  static Color colorWithRGBA(
      {required int r, required int g, required int b, double a = 1}) {
    return Color.fromARGB((255 * a).toInt(), r, g, b);
  }
}

Color randomColor() {
  return Color.fromARGB(
      255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255));
}
