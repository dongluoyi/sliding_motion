
import 'sliding_motion_platform_interface.dart';

class SlidingMotion {
  Future<String?> getPlatformVersion() {
    return SlidingMotionPlatform.instance.getPlatformVersion();
  }
}
