import 'dart:math';
import 'package:flutter/widgets.dart';

class LYArcPainterContainer extends StatelessWidget {
  final Color color;
  final Widget? child;
  final double radius;
  final Offset point;
  final double width;

  const LYArcPainterContainer(
      {super.key,
      required this.radius,
      this.color = const Color(0xffff00ff),
      this.child,
      this.point = Offset.zero,
      this.width = 4});

  @override
  Widget build(BuildContext context) {
    return point == Offset.zero
        ? SizedBox(
            child: child,
          )
        : CustomPaint(
            painter: LYArcPainter(
                color: color, radius: radius + width * 0.5, point: point, width: width),
            child: child,
          );
  }
}

class LYArcPainter extends CustomPainter {
  final Color color;
  final double radius;
  final Offset point;
  final double width;

  LYArcPainter(
      {required this.color, required this.radius, required this.point, required this.width});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = width
      ..strokeCap = StrokeCap.round;

    final centerX = size.width * 0.5;
    final centerY = size.height * 0.5;

    final angle = (180 / pi) * atan2((point.dy - centerY), (point.dx - centerX));
    final middleAngle = degreesToRadians(angle);
    final equalAngle = degreesToRadians(22);
    final path = Path();
    path.arcTo(
      Rect.fromCircle(center: Offset(centerX, centerY), radius: radius),
      middleAngle - equalAngle,
      2 * equalAngle,
      false,
    );
    canvas.drawPath(path, paint);
  }

  double degreesToRadians(double degrees) {
    return degrees * (pi / 180.0);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
